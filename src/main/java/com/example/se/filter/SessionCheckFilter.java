package com.example.se.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SessionCheckFilter implements Filter {

	FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
		
		HttpServletResponse rs = (HttpServletResponse) res;
		HttpServletRequest rq = (HttpServletRequest) req;
		
		String sessionAtrubuteValue;
		
		try {
			sessionAtrubuteValue = rq.getSession().getAttribute("login").toString();
		} catch (NullPointerException e) {
			sessionAtrubuteValue = null;
		}

		if(sessionAtrubuteValue != null){
			rs.sendRedirect("profile.jsp");
		}
		else
			chain.doFilter(req, res);
    }
    
    public void destroy() {
        //add code to release any resource
    }
}
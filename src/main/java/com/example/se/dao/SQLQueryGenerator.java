package com.example.se.dao;

import com.example.se.domain.Person;
import com.example.se.domain.address.Address;

public class SQLQueryGenerator {

	public String insert(Person person){
		String query = "insert into member (login, pass, email, permision) values ('" +
                person.getLogin() + "', '" +
                person.getPassword() + "', '" +
                person.getEmail() + "', '" +
                person.getPrivilages().name() + "');";
        return query;
	}
	
	public String update(Person person, String id){
		String query = "UPDATE MEMBER SET login='" + person.getLogin() + "', pass='" + person.getPassword() + "', email='" + person.getEmail()
				+ "', permision='" + person.getPrivilages().name() + "' WHERE id = '" + id + "'";
        return query;
	}
	
	public String delete(String id){
		return "DELETE FROM MEMBER WHERE ID = '" + id + "'";
	}
	
	public String ifExist(String login){
		return "SELECT COUNT(*) FROM MEMBER WHERE LOGIN = '" + login + "'";
	}
	
	public String selectLoginPass(String login, String pass){
		String query = "select id, login,pass,email,permision, count(id) as ilosc from member where login = '" + login + "' and pass = '" + pass + "' group by id";
		
		return query;
	}
	
	public String getAll(){
		return "select * from member";
	}
	

    public static String getIdByLogin(String login) {
        return "select id from member where login = '" + login +"';";
    }
    
    public static String getUserAddresses(String userid) {
        return "select * from address where userid = '" + userid + "';";
    }
    public static String deleteAddress(String id) {
        return "delete from address where Id = '" + id + "';";
    }
    
    public static String editAddress(Address address) {
        String query = "update address set " +
                "AddressType = '" + address.getType().toString() + "', " +
                "Province = '" + address.getProvince().toString() + "', " +
                "City = '" + address.getCity() + "', " +
                "ZipCode = '" + address.getZipCode() + "', " +
                "Street = '" + address.getStreet() + "', " +
                "House = '" + address.getHouseNumber() + "' " +
                "where Id = '" + address.getId() + "';";
        System.out.println(query);
        return query;
    }
    
    public static String addAddress(Address address, String userId) {
        String query = "insert into address (AddressType, Province, City, ZipCode, Street, House, UserId) values ('" +
                address.getType().toString() + "', '" +
                address.getProvince().toString() + "', '" +
                address.getCity() + "', '" +
                address.getZipCode() + "', '" +
                address.getStreet() + "', '" +
                address.getHouseNumber()+ "', '" +
                userId + "');";
        return query;
    }
}

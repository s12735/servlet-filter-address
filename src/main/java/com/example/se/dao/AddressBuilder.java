package com.example.se.dao;

import javax.servlet.http.HttpServletRequest;

import com.example.se.domain.address.Address;
import com.example.se.domain.address.Type;
import com.example.se.domain.address.Province;

public class AddressBuilder {

	public AddressBuilder() {
		// TODO Auto-generated constructor stub
	}
	
	public Address build(HttpServletRequest request){
		Address address = new Address();
		
		   if(request.getParameter("id") != null)
	            address.setId(Integer.parseInt(
	                    request.getParameter("id")));

	        address.setType(Type.valueOf(request.getParameter("type")));
	        address.setProvince(Province.valueOf(request.getParameter("province")));
	        address.setCity(request.getParameter("city"));
	        address.setZipCode(request.getParameter("zipCode"));
	        address.setStreet(request.getParameter("street"));
	        address.setHouseNumber(request.getParameter("house"));

	        return address;
	}
}

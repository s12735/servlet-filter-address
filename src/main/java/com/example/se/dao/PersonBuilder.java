package com.example.se.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.se.domain.Person;
import com.example.se.domain.Privilages;

public class PersonBuilder {

	public Person build(ResultSet rs) throws SQLException {
		Person p = new Person();
		p.setLogin(rs.getString(2));
		p.setPassword(rs.getString(3));
		p.setEmail(rs.getString(4));
		p.setPrivilages(Privilages.valueOf(rs.getString(5)));
		return p;
	}

	public Person build(String login, String pass, String email, String permision) {
		Person p = new Person();
		p.setLogin(login);
		p.setPassword(pass);
		p.setEmail(email);
		p.setPrivilages(Privilages.valueOf(permision));
		return p;
	}

}

package com.example.se.domain;

public class Person {
	private String login;
	private String password;
	private String email;
	private Privilages privilages;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Privilages getPrivilages() {
		return privilages;
	}
	public void setPrivilages(Privilages privilages) {
		this.privilages = privilages;
	}
	
	
}

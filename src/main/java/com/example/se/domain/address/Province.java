package com.example.se.domain.address;

public enum Province {
	Dolnoslaskie(1),
	KujawskoPomorskie(2),
	Lubelskie(3),
	Lubuskie(4),
	Łódzkie(5),
	Małopolskie(6),
	Mazowieckie(7),
	Opolskie(8),
	Podlaskie(9),
	Podkarpackie(10),
	Pomorskie(11),
	Śląskie(12),
	Świętokrzyskie(13),
	WarminskoMazurskie(14),
	Wielkopolskie(15),
	Zachodniopomorskie(16);
	
	 private int id;
	 private String name;

	private Province(String name){
		this.name = name;
	}
    private Province(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
    
    public String getName() {
		return this.name();
	}
}

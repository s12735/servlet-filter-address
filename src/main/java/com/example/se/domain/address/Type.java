package com.example.se.domain.address;

public enum Type {

	 Zameldowania(1), Korespondencyjny(2), Pracy(3);

	    private int id;

	    private Type(int id) {
	        this.id = id;
	    }

	    public int getId() {
	        return this.id;
	    }
}

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.se.dao.AddressBuilder;
import com.example.se.dao.DAO;
import com.example.se.domain.address.Address;

@WebServlet(urlPatterns= "/editAddress")
public class editAddress extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Address address = new AddressBuilder().build(request);
		
		String userLogin = request.getSession(false).getAttribute("login").toString();

		try {
			update(address, userLogin);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect("addressPage.jsp");

	}

	private void update(Address address, String login) throws SQLException {
		DAO db = null;
		try {
			db = new DAO();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (db != null)
			db.editAddress(address);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
}
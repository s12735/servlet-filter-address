<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.example.se.domain.address.Address"%>
<%@page import="java.util.Map"%>
<%@page import="com.example.se.dao.DAO"%>
<%@ page language="java" import="java.sql.*"%>

<html>
<head>
<style>
table {
	background-color: #ffe9bf;
	text-align: center;
}

input[type="text"] {
	background: none;
	border: none;
	text-align: center;
}
</style>

<title>Read from Database</title>
</head>
<body>

	<p align="center">
		<b>Registered Addresses of Member:</b><br>
	</p>
	<div align="center">
		<center>
			<table border="1">
				<tbody>
					<tr>
						<td style="display: none;"></td>

						<td bgColor="#008080"><font color="#ffffff"><b>Type</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>Province</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>City</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>Zip
									Code</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>Street</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>House
									number</b></font></td>
						<td bgColor="#008080"><font color="#ffffff"><b>Options</b></font></td>
					</tr>

					<%
						session.getAttribute("permision");

						DAO db = new DAO();
						String login = request.getSession().getAttribute("login").toString();
						Map<String, Address> lista;
						List<Address> addresses = db.getUserAddressesByLogin(login);

						// 						Class.forName("org.hsqldb.jdbcDriver").newInstance();
						// 						Connection con = null;
						// 						ResultSet rst = null;
						// 						Statement stmt = null;
						// 						try {
						// 							con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost", "sa", "");
						// 							stmt = con.createStatement();
						// 							rst = stmt.executeQuery("select * from member");
						for (Address a : addresses) {
					%>

					<form method='get'>
						<tr>
							<td style="display: none;"><input readonly="readonly" type='text' name='id' value="<%=a.getId()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='type' value="<%=a.getType()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='province' value="<%=a.getProvince()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='city' value="<%=a.getCity()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='zipCode' value="<%=a.getZipCode()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='street' value="<%=a.getStreet()%>" /><br></td>
							<td><input readonly="readonly" type='text' name='houseNumber' value="<%=a.getHouseNumber()%>" /><br></td>
							<td><input type="submit" ' name="submit" value='Edit'
								onclick="javascript: form.action='address.jsp';" />
								
								 <input	type="submit" name="submit" value="Delete"
								onclick="javascript: form.action='deleteAddress';" /></td>
					</form>
					</tr>
					<%
						}
					%>

				</tbody>
			</table>
			<a href="address.jsp">Add new address</a>
		</center>
	</div>
</body>
</html>